if(process.env.NODE_ENV !== 'production') require('dotenv').load();
var express = require('express');
var app = express();
var expressLayouts = require('express-ejs-layouts');

app.set('view engine', 'ejs');
app.set('views', './views');

app.use(expressLayouts);
var routes = require('./routes/manifest')(app);

app.use(express.static('public'));

app.listen(process.env.PORT || 3000);
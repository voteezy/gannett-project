var gannettRepo = require('../../repositories/gannett-repository');

module.exports = function(app) {
    app.get('/api/articles', function(req, res) {
        gannettRepo.getArticles(function(result) {
            res.json(result);
        });
    });

    app.get('/api/articleswithimages', function(req, res) {
        gannettRepo.getArticlesWithImages(function(result) {
            res.json(result);
        });
    });
}
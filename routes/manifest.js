module.exports = function(app) {
    require('./api/routes.js')(app);
    require('./main/routes.js')(app);
}
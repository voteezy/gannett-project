module.exports = function (app) {
    app.get('/', function(req, res) {
        res.render('mithril-materialize/index.ejs', { layout: 'mithril-materialize/_layout.ejs' });
    });
    
    app.get('/angular-bootstrap', function(req, res) {
        res.render('angular-bootstrap/index.ejs', { layout: 'angular-bootstrap/_layout.ejs' });
    });
}
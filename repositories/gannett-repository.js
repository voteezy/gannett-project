exports.getArticles = function(resolve) {
    gannettApi.getArticles(function(articles) {
        resolve(articles);    
    });
}

exports.getArticlesWithImages = function(resolve) {
    getArticlesWithImages(function(articles) {
       resolve(articles); 
    });
}

// private

var http = require('http');

var gannettApi = {
    getArticles: function(resolve) {
        http.get('http://api.gannett-cdn.com/ping/v1/most-popular/1?api_key=vp535c7g3m7tr3wzj8vehbaz&t=text', (response) => {
            var chunks = '';
            response.on('data', function(chunk) { 
                chunks += chunk;
            }).on('end', function() {
                resolve(JSON.parse(chunks));
            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e); // TO-DO
        });
    },
    getArticleDetails: function(ids, resolve) {       
        var uri = 'http://api.gannett-cdn.com/presentation/v4/assets/' + ids + '?api_key=tad8ms6as9m9tqm8m9kmw3jp&consumer=test&transform=mobile';
        
        http.get(uri, (response) => {
            var chunks = '';
            response.on('data', function(chunk) { 
                chunks += chunk;
            }).on('end', function() {
                resolve(JSON.parse(chunks));
            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e); // TO-DO
        });
    }
}

var getArticlesWithImages = function(resolve) {
    gannettApi.getArticles(function(articles) {
        var articleIds = '';
        articles.map(function(article, index, array) {
            articleIds += index === (array.length - 1) ? article.id : article.id + ',';
        });

        gannettApi.getArticleDetails(articleIds, function(articleDetails) {
            resolve(appendThumbnailUrlToArticles(articles, articleDetails));
        });
    });
}

var appendThumbnailUrlToArticles = function(articles, articleDetails) {
    articleDetails.map(function(articleDetail, index, array) {
       var thumbnailAssetId = articleDetail.thumbnailAsset;
       articleDetail.assets.forEach(function(asset) {
           if(asset.id === thumbnailAssetId) {
               articles[index].thumbnailUrl = asset.crops.front_thumb;
               return;
            }
       });
    });
    
    return articles;
};
angular.module('app', [])
.controller('NewsPageCtrl', function($scope, $http) {
    $scope.articles;
    
    $http({
        method: 'GET',
        url: '/api/articleswithimages',
        headers: { 'Content-Type': 'application/json' },
    }).then(function successCallback(response) {
        $scope.articles = response.data;
    }, function errorCallback(response) {
        console.log(response);
    });
});
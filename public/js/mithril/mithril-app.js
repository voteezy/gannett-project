var ApiClient = {
    newsArticles: function() {
        return m.request({
            method: 'GET',
            url: '/api/articleswithimages',
            config: function(xhr) { xhr.withCredentials = false; }
        });
    }
};

var MithrilComponent = function() {
    var viewModel = {
        
    };
    
    return {
        controller: function() {
            var newsArticles = ApiClient.newsArticles();
            return {
                viewModel: viewModel,
                newsArticles: newsArticles
            }
        },
        view: function(ctrl) {
            return [
                m('.row',
                    ctrl.newsArticles().map(function(article, index) {
                        return [
                            m('.col.l4.m6', {style: 'min-height: 280px;'}, [
                                m('.card',
                                    m('.card-image', [
                                        m('a', { href: article._links.longUrl.href, target: 'blank' }, [
                                            m('img', {src: article.thumbnailUrl}),
                                            m('span.card-title', {style: 'font-weight: 600;' }, article.title)
                                        ])
                                    ])
                                )
                            ])
                        ];
                    })
                ),
            ];
        }
    }
}

m.mount(document.getElementById('MithrilContainer'), MithrilComponent());